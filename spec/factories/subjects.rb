FactoryBot.define do
  factory :subject do
    name { "MyString" }
    theme_id { 1 }
  end
end
