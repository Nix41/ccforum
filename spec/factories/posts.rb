FactoryBot.define do
  factory :post do
    name { "MyString" }
    user_id { 1 }
    text { "MyText" }
    subject_id { 1 }
  end
end
