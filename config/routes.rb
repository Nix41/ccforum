Rails.application.routes.draw do
  root :to => 'home#index'
  mount_devise_token_auth_for 'User', at: 'auth'
  namespace :api do
    namespace :v1  do
      resources :subjects
      resources :comments
      resources :posts
      resources :themes
    end
  end

# constraints subdomain: 'api' do
#   scope module: 'api' do
#     namespace :v1 do
#       resources :subjects
#     end
#   end
# end

end