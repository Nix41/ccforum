# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'faker'

5.times do
    theme = Theme.create(name: Faker::Movies::LordOfTheRings.location)
    pass = Faker::Internet.password
    user = User.create(username: Faker::Movies::HarryPotter.character,email: Faker::Internet.email, password: pass, password_confirmation: pass)
    5.times do
        subject = Subject.create(name: Faker::Movies::LordOfTheRings.character)
        theme.subjects << subject
        subject.posts << Post.create(name: Faker::Movies::HarryPotter.spell , text: Faker::Movies::HarryPotter.quote, user_id: user.id)
    end
end

50.times do
    post = Post.all.sample
    user = User.all.sample
    post.comments << Comment.create(user_id: user.id, text: Faker::Movies::LordOfTheRings.quote)
end