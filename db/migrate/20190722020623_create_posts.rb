class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.string :name
      t.integer :user_id
      t.text :text
      t.integer :subject_id

      t.timestamps
    end
  end
end
