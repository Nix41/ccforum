class AddProfileToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :username, :string
    add_column :users, :Name, :string
    add_column :users, :Last_Names, :string
    add_column :users, :credits, :integer, default: 0
    add_column :users, :user_type, :integer
  end
end
