class ApplicationController < ActionController::API
  respond_to :json
  before_action :configure_permitted_parameters, if: :devise_controller?
  include DeviseTokenAuth::Concerns::SetUserByToken
  # protect_from_forgery with: :null_session
  # protect_from_forgery unless: -> { request.format.json? }
  # protect_from_forgery with: :null_session, only: Proc.new { |c| c.request.format.json? }  
  # before_action :authenticate_user! 
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:username, :Name, :Last_Name, :credits, :user_type])
  end
end
