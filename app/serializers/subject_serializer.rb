class SubjectSerializer < ActiveModel::Serializer
  attributes :id, :name, :theme_id
end
